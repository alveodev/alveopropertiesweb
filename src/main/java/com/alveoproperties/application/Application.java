package com.alveoproperties.application;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Application {

	private static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/*@Bean
	public CommandLineRunner loadData(PersonRepository user){
		return args -> {
		List<Person> personList = new ArrayList<Person>();

			personList.add(new Person("Rhea Zoe", "Frias", "rz@yahoo.com", "zowe", "123456","admin"));

			user.save(personList);

			List<Person> listFromDB = user.findAll();

			listFromDB.forEach(p -> {
				System.out.println(p.toString());
			});
		};
	}*/
	
}
